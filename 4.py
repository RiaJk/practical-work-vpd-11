def center_of_gravity_dsc() -> float:
    """
    Находит расстояние между центрами тяжести двух
прямоугольников.
    :return: расстояние
    """
    x1 = int(input('x1 = '))
    y1 = int(input('y1 = '))
    x2 = int(input('x2 = '))
    y2 = int(input('y2 = '))
    x3 = int(input('x3 = '))
    y3 = int(input('y3 = '))
    x4 = int(input('x4 = '))
    y4 = int(input('y4 = '))
    first_center_x = (x1 + x2) / 2
    first_center_y = (y1 + y2) / 2
    second_center_x = (x3 + x4) / 2
    second_center_y = (y3 + y4) / 2
    distance = ((second_center_x - first_center_x)**2 +
                (second_center_y - first_center_y)**2) ** 0.5
    print('Вычисления выполнены')
    return distance


def corner_distance() -> float:
    """
    Находит сумму расстояний между верхними левыми и
нижними правыми углами двух прямоугольников.
    :return: расстояние
    """
    x1 = int(input('x1 = '))
    y1 = int(input('y1 = '))
    x2 = int(input('x2 = '))
    y2 = int(input('y2 = '))
    x3 = int(input('x3 = '))
    y3 = int(input('y3 = '))
    x4 = int(input('x4 = '))
    y4 = int(input('y4 = '))
    first_x = (x2 - x1)
    first_y = (y2 - y1)
    second_x = (x4 - x3)
    second_y = (y4 - y3)
    distance = ((first_x**2 + first_y**2)**0.5 + (second_x**2 + second_y**2)**0.5)
    print('Вычисления выполнены')
    return distance


def main():
    while True:
        print("\n"
              "1 - расстояние между центрами "
              "тяжести двух прямоугольников \n"
              "2 - сумму расстояний между верхними левыми "
              "и нижними правыми углами двух прямоугольников \n"
              "0 - end \n")
        choice = input('Введите команду: ')
        if choice == '1':
            print(center_of_gravity_dsc())
            continue
        if choice == '2':
            print(corner_distance())
            continue
        if choice == '0':
            break
        else:
            print('введите верную команду!')


if __name__ == '__main__':
    main()
